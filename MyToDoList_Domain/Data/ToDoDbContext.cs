﻿using Microsoft.EntityFrameworkCore;
using MyToDoList_Domain.Models;
using System.Collections.Generic;

namespace MyToDoList_Domain.Data
{
    public class ToDoDbContext : DbContext
    {
        public DbSet<ToDoList> ToDoLists { get; set; }
        public DbSet<ToDoEntries> ToDoEntries { get; set; }
        public ToDoDbContext(DbContextOptions<ToDoDbContext> options) : base(options)
        {

        }
    }
}
