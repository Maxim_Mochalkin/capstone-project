﻿using Microsoft.EntityFrameworkCore;
using MyToDoList_Domain.Data;
using MyToDoList_Domain.Models;

namespace MyToDoList_Domain.Repository
{
    public class ToDoRepository : IToDoRepository
    {
        private readonly ToDoDbContext _context;

        public ToDoRepository(ToDoDbContext context)
        {
            _context = context;
        }

        public async Task AddNotesToEntryAsync(int entryId, string notes)
        {
            var entry = await _context.ToDoEntries.FindAsync(entryId);
            if (entry == null)
            {
                throw new ArgumentException("Invalid entry ID.");
            }

            entry.AdditionalNotes = notes;
            await _context.SaveChangesAsync();
        }

        //public async Task CopyListAsync(int id)
        //{
        //    var listToCopy = await GetListByIdAsync(id);
        //    if(listToCopy == null)
        //    {
        //        throw new ArgumentException("Invalid list ID.");
        //    }

        //    var newToDoList = new ToDoList
        //    {
        //        ToDoListName = listToCopy.ToDoListName,
        //        ToDoListDescription = listToCopy.ToDoListDescription,
        //        CreationTime = DateTime.Now,
        //        IsHidden = listToCopy.IsHidden,
        //    };

        //    foreach(var entry in listToCopy.ToDo ) { }
        //}

        public async Task CreateEntryAsync(ToDoEntries entry)
        {
            entry.CreatedTime = DateTime.Now;
            entry.ModifiedTime = DateTime.Now;
            await _context.ToDoEntries.AddAsync(entry);
            await _context.SaveChangesAsync();
        }

        public async Task CreateListAsync(ToDoList list)
        {
            list.CreationTime = DateTime.Now;
            await _context.ToDoLists.AddAsync(list);
            await _context.SaveChangesAsync();
        }

        public async Task CreateReminderAsync(int entryId, DateTime reminderDate)
        {
            var entry = await _context.ToDoEntries.FindAsync(entryId);
            if (entry != null)
            {
                entry.ReminderDate = reminderDate;
                await _context.SaveChangesAsync();
            }
        }

        public async Task DeleteEntryAsync(int id)
        {
            var entry = await GetEntryByIdAsync(id);
            if (entry == null)
            {
                throw new ArgumentException($"Entry with id {id} not found.");
            }
            _context.ToDoEntries.Remove(entry);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteListAsync(int id)
        {
            var list = await GetListByIdAsync(id);
            if (list == null)
            {
                throw new ArgumentException($"Entry with id {id} not found.");
            }
            _context.ToDoLists.Remove(list);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<ToDoEntries>> GetAllEntriesAsync()
        {
            return await _context.ToDoEntries.ToListAsync();
        }

        public async Task<IEnumerable<ToDoList>> GetAllListsAsync()
        {
            return await _context.ToDoLists.ToListAsync();
        }

        //public async Task<IEnumerable<ToDoEntries>> GetEntriesByListIdAsync(int listId)
        //{
        //    return await _context.ToDoEntries.Where(e => e.ListId == listId).ToListAsync();
        //}

        public async Task<IEnumerable<ToDoEntries>> GetEntriesDueTodayAsync()
        {
            var today = DateTime.Today;
            return await _context.ToDoEntries.Where(e => e.DueDate == today).ToListAsync();
        }

        public async Task<ToDoEntries> GetEntryByIdAsync(int id)
        {
            return await _context.ToDoEntries.FindAsync(id) ?? throw new NullReferenceException($"{id} not found.");
        }

        public async Task<ToDoList> GetListByIdAsync(int id)
        {
            return await _context.ToDoLists.FindAsync(id) ?? throw new NullReferenceException($"{id} not found.");
        }

        public async Task ChangeEntryStatusAsync(int id, Status status)
        {
            var entry = await _context.ToDoEntries.FindAsync(id);
            if (entry == null)
            {
                throw new ArgumentException("Invalid entry ID.");
            }

            entry.Status = status;
            await _context.SaveChangesAsync();
        }

        public async Task HideShowEntryAsync(int id, bool hide)
        {
            var entry = await _context.ToDoEntries.FindAsync(id);
            if (entry == null)
            {
                throw new ArgumentException("Invalid entry ID.");
            }

            entry.Hiden = hide;
            await _context.SaveChangesAsync();
        }

        public async Task HideShowListAsync(int id, bool hide)
        {
            var list = await _context.ToDoLists.FindAsync(id);
            if (list == null)
            {
                throw new ArgumentException("Invalid list ID.");
            }

            list.IsHidden = hide;
            await _context.SaveChangesAsync();
        }

        public async Task RemoveEntryAsync(int id)
        {
            var entry = await _context.ToDoEntries.FindAsync(id);
            if (entry == null)
            {
                throw new ArgumentException("Invalid entry ID.");
            }

            _context.ToDoEntries.Remove(entry);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveListAsync(int id)
        {
            var list = await _context.ToDoLists.FindAsync(id);
            if (list == null)
            {
                throw new ArgumentException("Invalid list ID.");
            }

            _context.ToDoLists.Remove(list);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateEntryAsync(ToDoEntries entry)
        {
            var existingEntry = await GetEntryByIdAsync(entry.Id);

            if (existingEntry != null)
            {
                existingEntry.ToDoList = entry.ToDoList;
                existingEntry.ToDoListId = entry.ToDoListId;
                existingEntry.ReminderDate = entry.ReminderDate;
                existingEntry.DueDate = entry.DueDate;
                existingEntry.Name = entry.Name;
                existingEntry.AdditionalNotes = entry.AdditionalNotes;
                existingEntry.Description = entry.Description;
                existingEntry.CreatedTime = entry.CreatedTime;
                existingEntry.ModifiedTime = DateTime.Now;
                existingEntry.Status = entry.Status;
                existingEntry.Hiden = entry.Hiden;
            }
            else
            {
                throw new ArgumentException("Entry was null");
            }
            _context.ToDoEntries.Update(existingEntry);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateListAsync(ToDoList list)
        {
            _context.ToDoLists.Update(list);
            await _context.SaveChangesAsync();
        }

        public async Task<List<ToDoEntries>> GetEntriesByListIdAsync(int? id)
        {
            var entries = await _context.ToDoEntries
                .Where(e => e.ToDoListId == id)
                .ToListAsync();

            return entries;
        }
    }
}
