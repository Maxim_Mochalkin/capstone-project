﻿using MyToDoList_Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyToDoList_Domain.Repository
{
    public interface IToDoRepository
    {
        Task<IEnumerable<ToDoList>> GetAllListsAsync();
        Task<ToDoList> GetListByIdAsync(int id);
        Task CreateListAsync(ToDoList list);
        Task UpdateListAsync(ToDoList list);
        Task DeleteListAsync(int id);
        Task<IEnumerable<ToDoEntries>> GetAllEntriesAsync();
        Task<ToDoEntries> GetEntryByIdAsync(int id);
        Task CreateEntryAsync(ToDoEntries entry);
        Task UpdateEntryAsync(ToDoEntries entry);
        Task DeleteEntryAsync(int id);
        Task<IEnumerable<ToDoEntries>> GetEntriesDueTodayAsync();
        //Task CopyListAsync(int id);
        Task HideShowListAsync(int id, bool hide);
        Task RemoveListAsync(int id);
        Task AddNotesToEntryAsync(int entryId, string notes);
        Task ChangeEntryStatusAsync(int id, Status status);
        Task HideShowEntryAsync(int id, bool hide);
        Task RemoveEntryAsync(int id);
        Task CreateReminderAsync(int entryId, DateTime reminderDate);
        Task<List<ToDoEntries>> GetEntriesByListIdAsync(int? id);
    }
}
