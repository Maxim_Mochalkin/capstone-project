﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyToDoList_Domain.Data;
using MyToDoList_Domain.Models;
using MyToDoList_Domain.Repository;

namespace ToDoListWebApplication.Controllers
{
    public class ToDoEntriesController : Controller
    {
        private readonly ToDoDbContext _context;
        private readonly IToDoRepository _repository;
        public ToDoEntriesController(ToDoDbContext context, IToDoRepository repository)
        {
            _context = context;
            _repository = repository;
        }

        // GET: ToDoEntries
        public async Task<IActionResult> Index()
        {
            var toDoListWebApplicationContext = _context.ToDoEntries.Include(t => t.ToDoList);
            return View(await toDoListWebApplicationContext.ToListAsync());
        }

        // GET: ToDoEntries/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var toDoEntries = await _context.ToDoEntries
                .Include(t => t.ToDoList)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDoEntries == null)
            {
                return NotFound();
            }

            return View(toDoEntries);
        }

        // GET: ToDoEntries/Create
        public IActionResult Create()
        {
            ViewData["ToDoListId"] = new SelectList(_context.Set<ToDoList>(), "Id", "ToDoListDescription");
            return View();
        }

        // POST: ToDoEntries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ToDoListId,Name,Description,Status,CreatedTime,ModifiedTime,AdditionalNotes,ReminderDate,DueDate,Hiden")] ToDoEntries toDoEntries)
        {
            if (!ModelState.IsValid)
            {
                await _repository.CreateEntryAsync(toDoEntries);
                return RedirectToAction(nameof(Index));
            }
            ViewData["ToDoListId"] = new SelectList(_context.Set<ToDoList>(), "Id", "ToDoListDescription", toDoEntries.ToDoListId);
            return View(toDoEntries);
        }

        // GET: ToDoEntries/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null || _context.ToDoEntries == null)
            {
                return NotFound();
            }

            var toDoEntries = await _context.ToDoEntries.FindAsync(id);
            if (toDoEntries == null)
            {
                return NotFound();
            }
            ViewData["ToDoListId"] = new SelectList(_context.Set<ToDoList>(), "Id", "ToDoListDescription", toDoEntries.ToDoListId);
            return View(toDoEntries);
        }

        // POST: ToDoEntries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ToDoListId,Name,Description,Status,ModifiedTime,AdditionalNotes,ReminderDate,DueDate,Hiden")] ToDoEntries toDoEntries)
        {

            if (!ModelState.IsValid)
            {
                try
                {
                    await _repository.UpdateEntryAsync(toDoEntries);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToDoEntriesExists(toDoEntries.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ToDoListId"] = new SelectList(_context.Set<ToDoList>(), "Id", "ToDoListDescription", toDoEntries.ToDoListId);
            return View(toDoEntries);
        }

        // GET: ToDoEntries/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            var toDoEntries = await _context.ToDoEntries
                .Include(t => t.ToDoList)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDoEntries == null)
            {
                return NotFound();
            }

            return View(toDoEntries);
        }

        // POST: ToDoEntries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.ToDoEntries == null)
            {
                return Problem("Entity set 'ToDoListWebApplicationContext.ToDoEntries'  is null.");
            }
            var toDoEntries = await _context.ToDoEntries.FindAsync(id);
            if (toDoEntries != null)
            {
                _context.ToDoEntries.Remove(toDoEntries);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ToDoEntriesExists(int id)
        {
          return (_context.ToDoEntries?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
