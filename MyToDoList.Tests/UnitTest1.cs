using Moq;
using MyToDoList_Domain.Models;
using MyToDoList_Domain.Repository;

namespace MyToDoList.Tests
{
    [TestFixture]
    public class ToDoRepositoryTests
    {
        private Mock<IToDoRepository> mockRepository;
        private IToDoRepository repository;

        [SetUp]
        public void Setup()
        {
            mockRepository = new Mock<IToDoRepository>();
            repository = mockRepository.Object;
        }

        [Test]
        public async Task GetAllListAsyncTest()
        {
            var mockLists = new List<ToDoList>
            {
                new ToDoList
                {
                    Id = 23232,
                    ToDoListName = "List 1",
                    ToDoListDescription = "Description 1",
                    CreationTime = DateTime.Now,
                    IsHidden = false
                },
                new ToDoList
                {
                    Id = 2,
                    ToDoListName = "List 2",
                    ToDoListDescription = "Description 2",
                    CreationTime = DateTime.Now,
                    IsHidden = true
                }
            };
            mockRepository.Setup(r => r.GetAllListsAsync()).ReturnsAsync(mockLists);

            // Act
            var result = await repository.GetAllListsAsync();
            var resultList = result.ToList();

            mockRepository.Verify(r => r.GetAllListsAsync(), Times.Once());
            Assert.That(resultList.Count, Is.EqualTo(mockLists.Count));

            Assert.That(resultList[0].ToDoListName, Is.EqualTo(mockLists[0].ToDoListName));
            Assert.That(resultList[1].IsHidden, Is.EqualTo(mockLists[1].IsHidden));
            Assert.That(mockLists[0].ToDoListDescription, Is.Not.EqualTo("My description"));
        }

        [Test]
        public async Task CreateListAsyncTest()
        {
            var newList = new ToDoList
            { Id = 1,
                ToDoListName = "My List",
                ToDoListDescription = "My List Description",
                CreationTime = DateTime.Now,
                IsHidden = false
            };
            bool isCallBackCalled = false;
            mockRepository.Setup(r => r.CreateListAsync(newList)).Callback((ToDoList list) =>
            {
                isCallBackCalled = true;
                Assert.NotNull(list);
                Assert.That(list.ToDoListName, Is.EqualTo("My List"));
                Assert.That(list.ToDoListName, Is.Not.EqualTo("List 22"));
            });
            await repository.CreateListAsync(newList);
            Assert.IsTrue(isCallBackCalled);
            mockRepository.Verify(r => r.CreateListAsync(It.IsAny<ToDoList>()), Times.Once);
        }

        [Test]
        public async Task CreateEnrtyAsyncTest()
        {
            var newEntry = new ToDoEntries
            {
                Id = 1,
                ToDoListId = 1,
                Name = "My Entry",
                Description = "My Entry Description",
                Status = Status.NotStarted,
                CreatedTime = DateTime.Now,
                ModifiedTime = null,
                AdditionalNotes = "",
                ReminderDate = null,
                DueDate = DateTime.Now.AddDays(7),
                Hiden = false
            };
            bool isCallBackCalled = false;
            mockRepository.Setup(r => r.CreateEntryAsync(newEntry)).Callback((ToDoEntries entry) =>
            {
                isCallBackCalled = true;
                Assert.NotNull(entry);
                Assert.That(entry.Name, Is.EqualTo("My Entry"));
                Assert.That(entry.Name, Is.Not.EqualTo("Not My Entry"));
            });
            await repository.CreateEntryAsync(newEntry);
            Assert.IsTrue(isCallBackCalled);
            mockRepository.Verify(r => r.CreateEntryAsync(It.IsAny<ToDoEntries>()), Times.Once);
        }

        [Test]
        public async Task UpdateListAsyncTest()
        {
            var updatedList = new ToDoList
            {
                Id = 1,
                ToDoListName = "Updated List",
                ToDoListDescription = "Description",
                CreationTime = DateTime.Now,
                IsHidden = false,
            };
            bool isCallBackCalled = false;
            mockRepository.Setup(r => r.UpdateListAsync(updatedList)).Callback((ToDoList list) =>
            {
                isCallBackCalled = true;
                Assert.NotNull(list);
                Assert.That(list.ToDoListName, Is.EqualTo("Updated List"));
                Assert.That(list.ToDoListName, Is.Not.EqualTo("List 22"));
            });
            await repository.UpdateListAsync(updatedList);
            Assert.IsTrue(isCallBackCalled);
            mockRepository.Verify(r => r.UpdateListAsync(It.IsAny<ToDoList>()), Times.Once);
        }

        [Test]
        public async Task UpdateEntryAsyncTest()
        {
            var updatedEntry = new ToDoEntries
            {
                Id = 1,
                ToDoListId = 1,
                Name = "Updated Entry",
                Description = "My Entry Description",
                Status = Status.NotStarted,
                CreatedTime = DateTime.Now,
                ModifiedTime = null,
                AdditionalNotes = "",
                ReminderDate = null,
                DueDate = DateTime.Now.AddDays(7),
                Hiden = false
            };

            bool isCallBackCalled = false;
            mockRepository.Setup(r => r.UpdateEntryAsync(updatedEntry)).Callback((ToDoEntries entry) =>
            {
                isCallBackCalled = true;
                Assert.NotNull(entry);
                Assert.That(entry.Name, Is.EqualTo("Updated Entry"));
                Assert.That(entry.Name, Is.Not.EqualTo("Not My Entry"));
            });
            await repository.UpdateEntryAsync(updatedEntry);
            Assert.IsTrue(isCallBackCalled);
            mockRepository.Verify(r => r.UpdateEntryAsync(It.IsAny<ToDoEntries>()), Times.Once);
        }
    }
}